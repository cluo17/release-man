#!/usr/bin/env bash
RELEASE_BRANCH=$1

if [ "$RELEASE_BRANCH" == "" ]; then
 echo "Usage: $0 <release_branch_name>"
 exit
fi
echo "checkout master branch ..."
git checkout master

echo "merge changes from $RELEASE_BRANCH ..."
git merge --no-ff "$RELEASE_BRANCH"

echo "tag the branch ${RELEASE_BRANCH:8}"
# "release/"  len: 8
git tag -a get_release_tag "${RELEASE_BRANCH:8}"

# merge the release branch back to the develop
echo "checkout develop branch ..."
git checkout develop

echo "merge changes from $RELEASE_BRANCH ..."
git merge --no-ff "$RELEASE_BRANCH"

echo "delete the release branch ...."
#git branch -d "$RELEASE_BRANCH"
